import React, { useRef, useState } from "react";

import { Form, Button, Spin, message } from 'antd';

import ApplicationApiService from "../../Service/ApplicationApiService";
import ImputGenerator from "../../Components/ImputGenerator/ImputGenerator";
import PageHeader from "../../Components/PageHeader/PageHeader";

/**
 * 
 * @param {*} props 
 * Number of imputs
 * optional number of obligated imputs
 */
export default function ApplicationPage(props) {


    let { nImputs, nObligatedImputs } = props

    const [postLoading, setPostLoading] = useState(false);

    const formRef = useRef(null);

    const api = new ApplicationApiService();

    const onFinish = (values) => {
        setPostLoading(true)
        console.log('Success:', values);
        formRef.current.resetFields();
        api.post(values).then((response) => {
            if (response.status === 200) {
                message.info("Lorem Ipsum cadastrado!")
            }
        }).catch((reason) => {
            message.error("Erro de comunicação com servidor!")
        }).finally(() => setPostLoading(false))
    };


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            name="basic"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            ref={formRef}>

            <PageHeader />

            <Spin spinning={postLoading}>
                <ImputGenerator nImputs={nImputs} nObligatedImputs={nObligatedImputs} />

                <Form.Item
                    style={
                        { textAlign: 'center' }
                    }
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Spin>


        </Form >)

}