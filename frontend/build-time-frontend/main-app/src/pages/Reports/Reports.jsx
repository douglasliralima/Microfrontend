import React, { useState, useEffect } from "react";

import { Table, Typography, Skeleton } from 'antd';

import PageHeader from "../../Components/PageHeader/PageHeader";
import InsightApi from "../../Service/InsightApi";

import './report.css'

const { Text } = Typography;


export default function Report() {
    const api = new InsightApi();

    const [data, setData] = useState([])
    const [columns, setColumns] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const columnsRequest = api.getReportColumns();
        const dataRequest = api.getReportData();

        Promise.allSettled([columnsRequest, dataRequest]).then((resolvedRequests) => {
            resolvedRequests.forEach((resolve, index) => {
                switch (index) {
                    case 0:
                        setColumns(resolve.value.data)
                    case 1:
                        setData(resolve.value.data)
                }
            })

            setLoading(false);
        })

    }, [])

    return (<>
        <PageHeader />

        {loading ? <Skeleton /> : <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            bordered
            summary={pageData => {

                const columnIndexList = columns
                    .filter((value) => value.dataIndex !== 'key' && value.dataIndex !== 'dia')
                    .map(value => value.dataIndex)

                const totalizer = Array(columnIndexList.length).fill().map(() => 0)

                pageData.forEach((pageColumns) => {
                    columnIndexList.forEach((value, i) => {
                        totalizer[i] += pageColumns[value]
                    })
                });

                return (
                    <>
                        <Table.Summary.Row>
                            <Table.Summary.Cell>Total</Table.Summary.Cell>
                            {totalizer.map((value) => <>
                                <Table.Summary.Cell>
                                    <Text type="danger">{value}</Text>
                                </Table.Summary.Cell>
                            </>)}
                        </Table.Summary.Row>
                    </>
                );
            }}
        />}

    </>)
}