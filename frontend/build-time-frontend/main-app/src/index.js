import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App"

import 'antd/dist/antd.css';

const mountNode = document.getElementById('app');

ReactDOM.render(<App />, mountNode);
