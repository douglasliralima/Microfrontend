export default {
    home: '/',
    application: {
        index: '/application-pages',
        pageA:  {
            title: "Página de Aplicação A",
            index: `/application-pages/a`,
        },
        pageB: {
            title: "Página de Aplicação B",
            index: `/application-pages/b`,
        },
    },
    insight: {
        index: '/insight',
        reports: {
            title: "Relatórios",
            index: '/insight/reports',
        } ,
        dashboard:  {
            title: "Dashboards",
            index: '/insight/dashboard',
        } ,
    },
}