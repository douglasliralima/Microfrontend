import React from 'react';

import Template from './pages/Template/Template';

import "./App.css"
import { BrowserRouter } from 'react-router-dom';

const App = () => {
    return <div className="app">
        <BrowserRouter>
            <Template />
        </BrowserRouter>
    </div>
}

export default App;