const path = require('path')

const libName = 'insight-client';

//Para a library funcionar, cada uma das libraries externas precisam ser linkadas na aplicação principal
const externals = [
    'react',
    'react-dom',
    'antd'
];

module.exports = {
    entry: "./src/index.js",
    mode: "production",
    output: {
        path: path.resolve(__dirname, "lib"),
        filename: "index.js",
        publicPath: '/lib/',
        library: libName,
        libraryTarget: 'umd',
    },
    module: {
        rules: [
            {
                test: /\.css/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(js|jsx)$/,
                use: ["babel-loader"],
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ["", ".jsx", ".js"]
    },
    externals: externals
}