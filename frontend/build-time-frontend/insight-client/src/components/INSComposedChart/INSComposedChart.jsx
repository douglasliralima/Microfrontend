import React, { useEffect, useState } from 'react';
import {
  ResponsiveContainer,
  ComposedChart,
  Line,
  Area,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';
import { Skeleton } from 'antd';

import InsightApi from "./../../Service/InsightApi"

export default function INSComposedChart() {

  const api = new InsightApi();

  const [data, setData] = useState([]);

  useEffect(async () => {
    const request = await api.getComposedData()
    setData(request.data)
  }, [])

  return (
    data.length === 0 ? <Skeleton /> :
    <div style={{ width: '100%', height: 300 }}>
      <ResponsiveContainer>
        <ComposedChart
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" />
          <XAxis dataKey="name" scale="band" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Area type="monotone" dataKey="renda" fill="#8884d8" stroke="#8884d8" />
          <Bar dataKey="custoCalculado" barSize={20} fill="#413ea0" />
          <Line type="monotone" dataKey="custoCorrigido" stroke="#ff7300" />
        </ComposedChart>
      </ResponsiveContainer>
    </div>
  );
}
