const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

const path = require('path')

const hostName = 'insight_client';

//Para a library funcionar, cada uma das libraries externas precisam ser linkadas na aplicação principal
const externals = [
    'react',
    'react-dom',
    'antd'
];

module.exports = {
    entry: "./src/index.js",
    mode: "production",
    devServer: {
        static: path.join(__dirname, 'lib'),
        port: 4000,
    },
    output: {
        publicPath: 'auto',
    },
    module: {
        rules: [
            {
                test: /\.css/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(js|jsx)$/,
                use: ["babel-loader"],
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ["", ".jsx", ".js"]
    },
    plugins: [
        new ModuleFederationPlugin({
            name: hostName,
            library: { type: 'var', name: hostName },
            filename: 'remoteEntry.js',
            exposes: {
                './INSDashboard': './src/pages/INSDashboard/INSDashboard',
                './SimpleComponent': './src/components/SimpleComponent/SimpleComponent',
            },
            shared: {
                react: { singleton: true, import: false },
                'react-dom': { singleton: true, import: false },
                'antd': { singleton: true, import: false }
            },
        })
    ]
}