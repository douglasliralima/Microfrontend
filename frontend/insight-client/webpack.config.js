const config = process.env.NODE_ENV === 'microfrontend' ? require('./webpack.host') : require('./webpack.build');

module.exports = {
  ...config,
  mode: 'development',
  devtool: 'eval',
};
