import React, { useEffect, useState } from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import { Skeleton } from 'antd';

import InsightApi from "./../../Service/InsightApi"

export default function INSRadarChart() {

  const api = new InsightApi();

  const [data, setData] = useState([]);

  useEffect(async () => {
    const request = await api.getRadarData()
    setData(request.data)
  }, [])

  return (
    data.length === 0 ? <Skeleton /> :
      <div style={{ width: '100%', height: 500 }}>
        <ResponsiveContainer>
          <RadarChart cx="50%" cy="50%" outerRadius="80%" data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="subject" />
            <PolarRadiusAxis />
            <Radar name="Mike" dataKey="A" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
          </RadarChart>
        </ResponsiveContainer>
      </div>
  );
}
