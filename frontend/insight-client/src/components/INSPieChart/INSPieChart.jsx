import React, { useState, useEffect } from 'react';
import { Skeleton } from 'antd';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';

import InsightApi from "./../../Service/InsightApi"

export default function INSPieChart() {

const renderActiveShape = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  startAngle,
  endAngle,
  fill,
  payload,
  percent
}) => {
  const RADIAN = Math.PI / 180;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      {/* Inner Text */}
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {`${payload.name} - ${(percent * 100).toFixed(0)}%`}
      </text>
      {/* Active Semi-Circle */}
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      {/* Outer Semi-Circle */}
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
    </g>
  );
};


  const api = new InsightApi();

  const [activeIndex, setActiveIndex] = useState(0);
  const [data, setData] = useState([]);

  useEffect(async () => {
    const request = await api.getPieData()
    setData(request.data)
  }, [])


  const onPieEnter = (data, index) => {
    setActiveIndex(index);
  };


  return (
    data.length === 0 ? <Skeleton/> : 
    <div style={{ width: '100%', height: '100%' }}>
      <ResponsiveContainer >
        <PieChart >
          <Pie
            activeIndex={activeIndex}
            activeShape={renderActiveShape}
            data={data}
            cx="50%"
            cy="50%"
            innerRadius='25%'
            outerRadius='80%'
            labelLine={false}
            fill="#8884d8"
            dataKey="value"
            onMouseEnter={onPieEnter}
          >
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={data[index % data.length].color} />
            ))}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    </div>
  );
}
