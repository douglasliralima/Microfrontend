import React from "react";
import { Row, Col } from 'antd';
import INSPieChart from "../../components/INSPieChart/INSPieChart";
import INSRadarChart from "../../components/INSRadarChart/INSRadarChart"
import INSComposedChart from "../../components/INSComposedChart/INSComposedChart";

export default function INSDashboard(props) {
    const { } = props;

    return (
        <div>
            <Row >
                <Col span={12} >
                    <INSPieChart />
                    <div id="borderLeft" style={{
                        borderLeft: '1px solid #ccc',
                        position: 'absolute',
                        top: '25%',
                        right: '0',
                        bottom: '0',
                    }}
                    />
                </Col>
                <Col span={12}>
                    <INSRadarChart />
                </Col>
            </Row>
            <Row style={{ borderTop: '1px solid #ccc' }}>
                <INSComposedChart />
            </Row>

        </div>

    )
}