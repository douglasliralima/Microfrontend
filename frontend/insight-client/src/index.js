import INSDashboard from './pages/INSDashboard/INSDashboard'
import INSComposedChart from './components/INSComposedChart/INSComposedChart'
import INSPieChart from './components/INSPieChart/INSPieChart'
import INSRadarChart from './components/INSRadarChart/INSRadarChart'
import SimpleComponent from './components/SimpleComponent/SimpleComponent'

export {INSDashboard, INSComposedChart, INSPieChart, INSRadarChart, SimpleComponent}
