import axios from "axios";
import endpoints from "../Constants/endpoints";

export default class InsightApi {
    apiUrl

    constructor(){
        this.apiUrl = endpoints.insightAPI;
    }

    getPieData(){
        return axios.get(`${this.apiUrl}/chart/pie`)
    }

    getRadarData(){
        return axios.get(`${this.apiUrl}/chart/radar`)
    }

    getComposedData(){
        return axios.get(`${this.apiUrl}/chart/composed`)
    }

    getReportColumns(){
        return axios.get(`${this.apiUrl}/reports/columns`)
    }

    getReportData(){
        return axios.get(`${this.apiUrl}/reports/data`)
    }

}