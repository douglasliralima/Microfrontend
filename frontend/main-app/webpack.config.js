const path = require('path')
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const plugins = [
    new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src/index.html'),
        filename: 'index.html',
        inject: 'body',
    }),
    new ModuleFederationPlugin({
        name: '[name]',
        remotes: {
            insight_client: 'insight_client@http://127.0.0.1:4000/remoteEntry.js',
        },
        shared: {
            react: { singleton: true, eager: true },
            'react-dom': { singleton: true, eager: true },
            'antd': { singleton: true, eager: true }
        },

    }),
]


module.exports = {
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index.bundle.js',
        publicPath: '/'
    },
    devServer: {
        historyApiFallback: true,
        port: 3000,
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
    },
    plugins: plugins,
}