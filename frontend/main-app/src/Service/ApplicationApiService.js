import axios from "axios";
import endpoints from "../Constants/endpoints";

export default class ApplicationApiService {
    
    apiUrl

    constructor(){
        this.apiUrl = endpoints.baseApplicationAPI;
    }

    get() {
        return axios.get(this.apiUrl)
    }

    post(data) {
        return axios.post(this.apiUrl, data)
    }
}