import { Divider } from "antd";
import React from "react";
import UrlRoutes from "../../Constants/urlRoutes";

export default function PageHeader() {
    const getPageTitle = () => {
        let pageTitle;

        Object.keys(UrlRoutes).forEach((routes) => {
            Object.keys(UrlRoutes[routes]).forEach((route) => {
                if (UrlRoutes[routes][route].index === window.location.pathname) {
                    pageTitle = UrlRoutes[routes][route].title
                }
            })
        })

        return pageTitle
    }

    return <>
        <h1>{getPageTitle()}</h1>
        <Divider />
    </>
}