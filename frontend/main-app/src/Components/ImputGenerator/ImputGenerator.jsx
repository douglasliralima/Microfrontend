import React from "react";

import { Form, Input } from 'antd';

const ImputGenerator = (props) => {
    let { nImputs, nObligatedImputs } = props
    const imputs = []
    let required
    for (let i = 0; i < nImputs; i++) {
        console.log(i)
        required = false;

        if (nObligatedImputs) {
            required = true
            nObligatedImputs--
        }

        imputs.push(<Form.Item
            label={`Lorem ipsum`}
            name={`Lorem ipsum ${i}`}
            rules={[
                {
                    required: required,
                    message: 'Campo obrigatório.',
                },
            ]}

            style={{
                display: 'inline-block', minWidth: "calc(33% - 8px)",
                maxWidth: "calc(50% - 8px)", marginRight: '8px'
            }}
        >

            <Input />
        </Form.Item>)
    }
    console.log(imputs)
    return <>
        {imputs}
    </>
}


export default ImputGenerator;