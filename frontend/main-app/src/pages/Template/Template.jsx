import React from 'react';

import { Layout, Menu } from 'antd';

import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    FormOutlined,
    DownloadOutlined,
    PieChartOutlined,
    HomeOutlined,
    ContainerOutlined,
    FileTextOutlined

} from '@ant-design/icons';

import { Link } from "react-router-dom";

import UrlRoutes from "../../Constants/urlRoutes";

import IndexRoutes from '../../routes/routes';

const { Header, Content, Sider } = Layout;

export default class Template extends React.Component {
    state = {
        collapsed: false,
    };

    openSideBar = () => {
        this.setState({
            collapsed: false,
        });
    }

    closeSideBar = () => {
        this.setState({
            collapsed: true,
        });
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    SideMenu = () => {
        return <Sider trigger={null}
            collapsible
            collapsed={this.state.collapsed}
            onMouseEnter={this.openSideBar}
            onMouseLeave={this.closeSideBar}
            width='max-content'>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1" icon={<HomeOutlined />}>
                    <Link to={UrlRoutes.home}>
                        Cockpit - Gráfico
                    </Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<PieChartOutlined />}>
                    <Link to={UrlRoutes.insight.dashboard.index}>
                        Dashboard - Gráfico
                    </Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<FormOutlined />}>
                    <Link to={UrlRoutes.application.pageA.index}>
                        Application Page A
                    </Link>
                </Menu.Item>
                <Menu.Item key="4" icon={<FormOutlined />}>
                    <Link to={UrlRoutes.application.pageB.index}>
                        Application Page B
                    </Link>
                </Menu.Item>
                <Menu.Item key="5" icon={<ContainerOutlined />}>
                    <Link to={UrlRoutes.insight.reports.index}>
                        Reports - Tabela
                    </Link>
                </Menu.Item>
                {/* <Menu.Item key="6" icon={<DownloadOutlined />}>
                    <Link to={UrlRoutes.download.index}>
                        Download - Tabela
                    </Link>
                </Menu.Item> */}
            </Menu>
        </Sider>
    }

    render() {
        const { SideMenu } = this

        return <Layout style={{minHeight:'100%'}}>

            <SideMenu />

            <Layout className="site-layout">

                <Header style={{ padding: 0, backgroundColor: "#004372" }} >
                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        style: { margin: 20 },
                        className: 'trigger',
                        onClick: this.toggle,
                    })}
                </Header>
                <Content
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                    }}
                >
                    <IndexRoutes />
                </Content>

            </Layout>
        </Layout >
    }
}