import React from 'react'
import { Route, Switch } from 'react-router'

import UrlRoutes from '../Constants/urlRoutes'

import SimpleComponent from 'insight_client/SimpleComponent';

import ApplicationPage from '../pages/ApplicationPage/ApplicationPage'
import Dashboard from '../pages/Dashboard/Dashboard'
import Report from '../pages/Reports/Reports'

export default class IndexRoutes extends React.Component {

    render() {
        return (
            <Switch>
                <Route
                    render={() => <h1>
                        <SimpleComponent />

                        Initial Screen

                    </h1>}
                    exact
                    path={UrlRoutes.home}
                />
                <Route
                    render={() => <ApplicationPage nImputs={4} nObligatedImputs={2} />}
                    path={UrlRoutes.application.pageA.index}
                />
                <Route
                    render={() => <ApplicationPage nImputs={7} nObligatedImputs={2} />}
                    path={UrlRoutes.application.pageB.index}
                />
                <Route
                    render={() => <Dashboard />}
                    path={UrlRoutes.insight.dashboard.index}
                />
                <Route
                    render={() => <Report />}
                    path={UrlRoutes.insight.reports.index}
                />
            </Switch>)
    }
}