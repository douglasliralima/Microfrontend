const mockdata = [
    {
        "Loren Ipsun 1": "Lorem ipsum.",
        "Loren Ipsun 2": "Lorem ipsum dolor sit amet.",
        "Loren Ipsun 3": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Loren Ipsun 4": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consequat odio id dolor convallis ultricies."
    },
    {
        "Loren Ipsun 1": "Lorem ipsum.",
        "Loren Ipsun 2": "Lorem ipsum dolor sit amet.",
        "Loren Ipsun 3": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Loren Ipsun 4": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consequat odio id dolor convallis ultricies."
    }
];

export default mockdata;