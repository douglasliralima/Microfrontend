import express from "express";
import cors from "cors";
import mockdata from "./constant/mockdata"

const app = express()

const port = 3001
const data = mockdata

app.use(cors());

app.use(express.json())

app.get('/', (req, res) => {
    res.send(data);
})

app.post('/', (req, res) => {
    data.push(req.body)
    res.status(200).send("Data Updated")
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})