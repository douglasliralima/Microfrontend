import express from "express";
import cors from "cors";

import reportsRouter from "./routes/reportsRouter";
import chartRouter from "./routes/chartRouter";

const app = express()

const port = 3002

app.use(cors());

app.use(express.json())

app.use(reportsRouter)

app.use(chartRouter)

app.listen(port, () => {
    console.log(`Insight-client API is listening at http://localhost:${port}`)
})