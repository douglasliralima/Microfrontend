import express from 'express';

import mockpie from '../constant/mockpie';
import mockradar from '../constant/mockradar';
import mockcomposed from '../constant/mockcomposed';

const chartRouter = express.Router()

chartRouter.get('/chart/radar', (req, res) => {
    res.send(mockradar)
});

chartRouter.get('/chart/pie', (req, res) => {
    res.send(mockpie)
});

chartRouter.get('/chart/composed', (req, res) => {
    res.send(mockcomposed)
});

export default chartRouter;