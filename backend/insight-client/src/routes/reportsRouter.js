import express from 'express';

import { getRandomInt } from '../utils';

const reportsRouter = express.Router()

const columnsName = ['dia', 'pagamentos', 'vendas', 'aliquotas', 'demanda', 'debitos', 'creditos', 'declarado']

const columnsTotalValue = {
    pagamentos: 312,
    vendas: 323,
    aliquotas: 234,
    demanda: 463,
    debitos: 374,
    creditos: 618,
    declarado: 541
}

const numberOfLines = 14

reportsRouter.get('/reports/columns', (req, res) => {
    const columns = []
    columnsName.forEach((name) => {
        columns.push({
            title: name[0].toUpperCase() + name.slice(1),
            dataIndex: name
        })
    });

    res.send(columns);
})

reportsRouter.get('/reports/data', (req, res) => {
    const data = []
    const columnsCalculedValue = JSON.parse(JSON.stringify(columnsTotalValue));
    for (let i = 0; i < numberOfLines; i++) {

        const newLine = {}

        columnsName.forEach((value) => {
            const lowValue = Math.ceil(columnsCalculedValue[value] * (numberOfLines / 100))
            newLine['key'] = i
            newLine['dia'] = new Date(`2019-02-${i+1}`).toISOString().substring(0, 10)
            newLine[value] = getRandomInt(1, lowValue);
            columnsCalculedValue[value] -= lowValue;
        })
        
        data.push(newLine)
    }
    res.send(data)
})

export default reportsRouter;