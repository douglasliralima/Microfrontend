export default [
    { name: 'Grupo A', value: 400, color: '#0088FE' },
    { name: 'Grupo B', value: 300, color: '#00C49F' },
    { name: 'Grupo C', value: 300, color: '#FFBB28' },
    { name: 'Grupo D', value: 200, color: '#FF8042' },
  ];