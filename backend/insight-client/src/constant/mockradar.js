export default [
    {
        subject: 'Filial A',
        A: 120,
        B: 110,
        fullMark: 150,
    },
    {
        subject: 'Filial B',
        A: 98,
        B: 130,
        fullMark: 150,
    },
    {
        subject: 'Filial C',
        A: 86,
        B: 130,
        fullMark: 150,
    },
    {
        subject: 'Filial D',
        A: 99,
        B: 100,
        fullMark: 150,
    },
    {
        subject: 'Filial E',
        A: 120,
        B: 110,
        fullMark: 150,
    },
    {
        subject: 'Filial F',
        A: 98,
        B: 130,
        fullMark: 150,
    },
];
