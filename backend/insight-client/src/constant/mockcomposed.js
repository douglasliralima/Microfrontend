export default [
    {
      name: 'Quadrimestre A',
      renda: 590,
      custoCalculado: 813,
      custoCorrigido: 689,
    },
    {
      name: 'Quadrimestre B',
      renda: 868,
      custoCalculado: 1267,
      custoCorrigido: 912,
    },
    {
      name: 'Quadrimestre C',
      renda: 1397,
      custoCalculado: 1598,
      custoCorrigido: 1489,
    },
    {
      name: 'Quadrimestre D',
      renda: 1480,
      custoCalculado: 1200,
      custoCorrigido: 1228,
    },
    {
      name: 'Quadrimestre E',
      renda: 1520,
      custoCalculado: 1108,
      custoCorrigido: 950,
    },
    {
      name: 'Quadrimestre F',
      renda: 1400,
      custoCalculado: 680,
      custoCorrigido: 572,
    },
  ];